package com.rawsur.apigticket.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Immutable;

@Entity
@Table(name = "v_historique_ticket")
@Immutable
public class HistoriqueTicket implements Serializable {

  @Id
  private Long id_historique;

  @Column(name = "id_ticket")
  private int id_ticket;

  @Column(name = "objet")
  private String objet;

  @Column(name = "title_it")
  private String title_it;

  @Column(name = "title_requestor")
  private String title_requestor;

  @Column(name = "description_requestor")
  private String description_requestor;

  @Column(name = "description_it")
  private String description_it;

  @Column(name = "criticality_requestor")
  private int criticality_requestor;

  @Column(name = "criticality_it")
  private int criticality_it;

  @Column(name = "date_create")
  private LocalDateTime date_create;

  @Column(name = "date_resolve")
  private LocalDateTime date_resolve;

  @Column(name = "date_update_ticket")
  private LocalDateTime date_update_ticket;

  @Column(name = "status")
  private String status;

  @Column(name = "observation")
  private String observation;

  @Column(name = "delai_resolution_requestor")
  private Double delai_resolution_requestor;

  @Column(name = "delai_resolution_it")
  private Double delai_resolution_it;

  @Column(name = "name_type_incident_it")
  private String name_type_incident_it;

  @Column(name = "name_type_incident_requestor")
  private String name_type_incident_requestor;

  @Column(name = "name_priority_it")
  private String name_priority_it;

  @Column(name = "name_priority_requestor")
  private String name_priority_requestor;

  @Column(name = "name_user_update")
  private String name_user_update;

  @Column(name = "name_user_create")
  private String name_user_create;

  @Column(name = "name_requestor")
  private String name_requestor;
}
