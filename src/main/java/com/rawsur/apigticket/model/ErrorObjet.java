package com.rawsur.apigticket.model;

import lombok.Data;

@Data
public class ErrorObjet {
  private String errorCode;
  private String errorDescription;

  public ErrorObjet(String errorCode, String errorDescription) {
    this.errorCode = errorCode;
    this.errorDescription = errorDescription;
  }
}
