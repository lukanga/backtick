package com.rawsur.apigticket.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;

import lombok.Data;

@Data
@Entity
@Table(name = "ticket")
public class Ticket {
   @Id
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   private int id;

   @Column()
   private String objet;
   @Column()
   private String title_it;
   @Column()
   private String title_requestor;
   @Column()
   private String description_requestor;
   @Column()
   private String description_it;
   @Column()
   private int criticality_requestor;
   @Column()
   private int criticality_it;
   @Column()
   @CreationTimestamp
   private LocalDateTime date_create;

   @Column()
   private LocalDateTime date_resolve;
   @Column()
   private String status;
   @Column()
   private String observation;
   @Column()
   private Double delai_resolution_requestor;

   @Column
   private Double delai_resolution_it;

   @Column()
   private String capture_incident;

   @OneToOne()
   @JoinColumn(name = "id_type_incident_it")
   private TypeIncident type_incident_it;
   @OneToOne()
   @JoinColumn(name = "id_type_incident_requestor")
   private TypeIncident type_incident_requestor;
   @OneToOne()
   @JoinColumn(name = "id_priority_requestor")
   private Priority priority_requestor;
   @OneToOne()
   @JoinColumn(name = "id_priority_it")
   private Priority priority_it;
   @OneToOne()
   @JoinColumn(name = "id_requestor")
   private Requestor requestor;
   @OneToOne()
   @JoinColumn(name = "id_user_create")
   private User user_create;
   @OneToOne()
   @JoinColumn(name = "id_user_update")
   private User user_update;

   public Ticket() {
   }

}
