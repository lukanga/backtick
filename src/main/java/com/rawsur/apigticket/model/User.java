package com.rawsur.apigticket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "user")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column()
    private String email;
    @Column()
    private String password;
    @Column()
    private String names;
    @Column()
    private int profil;

    @ManyToOne
    @JoinColumn(name = "id_direction")
    private Direction direction;

    public User() {
    }

    public User(int id) {
        this.id = id;
    }

}
