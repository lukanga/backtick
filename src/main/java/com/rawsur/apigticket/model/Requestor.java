package com.rawsur.apigticket.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "requestor")
public class Requestor {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column()
    private String names;

    @Column()
    private int id_user;

    public Requestor() {
    }

    public Requestor(int id) {
        this.id = id;
    }
}
