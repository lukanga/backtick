package com.rawsur.apigticket.enumaration;

public enum StatusTicket {
    OUVERT("Ouvert"),
    RESOLU("Resolu"),
    SUSPENS("Suspens"),
    CLOTURE("Cloture");

    private String value;

    StatusTicket(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }
}