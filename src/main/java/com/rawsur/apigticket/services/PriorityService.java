package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.Priority;
import com.rawsur.apigticket.repository.PriorityRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service
public class PriorityService {
  @Autowired
  private PriorityRepository priorityRepository;

  public List<Priority> getAllPriority() {
    return priorityRepository.findAllByOrderByIdDesc();
  }

  public Optional<Priority> getOnePriority(Integer id) {
    return priorityRepository.findById(id);
  }

  public ResponseEntity<Priority> createPriority(Priority priority) {
    Priority save = priorityRepository.save(priority);
    return new ResponseEntity<Priority>(save, HttpStatus.CREATED);
  }

  public Priority updatePriority(Priority priority) {
    Priority priorityFound = priorityRepository.findById(priority.getId())
        .orElseThrow(() -> new RuntimeException("Priority not Found"));
    if (priorityFound != null) {
      priorityFound.setId(priority.getId());
      priorityFound.setName(priority.getName());

      return priorityRepository.save(priority);
    }
    return null;
  }
}
