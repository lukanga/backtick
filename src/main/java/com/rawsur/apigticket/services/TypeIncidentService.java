package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.TypeIncident;
import com.rawsur.apigticket.repository.TypeIncidentRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TypeIncidentService {
  @Autowired
  private TypeIncidentRepository typeIncidentRepository;

  public List<TypeIncident> getAllTypeIncident() {
    return typeIncidentRepository.findAllByOrderByIdDesc();
  }

  public Optional<TypeIncident> getOneTypeIncident(Integer id) {
    return typeIncidentRepository.findById(id);
  }

  public TypeIncident createTypeIncident(TypeIncident typeIncident) {
    return typeIncidentRepository.save(typeIncident);
  }

  public TypeIncident updateTypeIncident(TypeIncident incident) {
    TypeIncident typeIncidentFound = this.typeIncidentRepository.findById(incident.getId())
        .orElseThrow(() -> new RuntimeException("Type incident not Found"));
    if (typeIncidentFound != null) {
      typeIncidentFound.setId(incident.getId());
      typeIncidentFound.setName(incident.getName());

      return typeIncidentRepository.save(typeIncidentFound);
    }
    return null;
  }
}
