package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.dto.CreateDirection;
import com.rawsur.apigticket.dto.UpdateDirectionDto;
import com.rawsur.apigticket.model.Direction;
import com.rawsur.apigticket.model.Site;
import com.rawsur.apigticket.repository.DirectionRepository;
import com.rawsur.apigticket.repository.SiteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DirectionService {
  @Autowired
  private DirectionRepository directionRepository;

  @Autowired
  private SiteRepository siteRepository;

  public List<Direction> getAllDirections() {
    return this.directionRepository.findAllByOrderByIdDesc();
  }

  public List<Direction> getDirectionsByIdSite(int id_site) {
    return this.directionRepository.findByIdSite(id_site);
  }

  public Optional<Direction> getOneSiteById(int id) {
    return this.directionRepository.findById(id);
  }

  public Direction creatDirection(CreateDirection directionDto) {

    Site site = this.siteRepository.findById(directionDto.getId_site())
        .orElseThrow(() -> new RuntimeException("Site not Found"));

    Direction direction = new Direction();
    direction.setName(directionDto.getName());
    direction.setSite(site);
    return this.directionRepository.save(direction);
  }

  public Direction updateSite(UpdateDirectionDto directionDto) {
    Direction direction = this.directionRepository.findById(directionDto.getId())
        .orElseThrow(() -> new RuntimeException("Direction not Found"));
    direction.setName(directionDto.getName());
    Site site = this.siteRepository.findById(directionDto.getId_site())
        .orElseThrow(() -> new RuntimeException("Site not Found"));
    direction.setSite(site);
    return this.directionRepository.save(direction);
  }
}
