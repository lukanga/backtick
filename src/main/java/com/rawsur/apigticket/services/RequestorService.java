package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.Requestor;
import com.rawsur.apigticket.repository.RequestorRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RequestorService {
  @Autowired
  private RequestorRepository requestorRepository;

  public List<Requestor> getAllRequestor() {
    return requestorRepository.findAll();
  }

  public Optional<Requestor> getOneRequestor(final Integer id) {
    return requestorRepository.findById(id);
  }

  public Requestor createRequestor(Requestor requestor) {
    return requestorRepository.save(requestor);
  }

  public Requestor updateRequestor(Requestor requestor) {
    Requestor requestorerFound = this.requestorRepository.findById(requestor.getId())
        .orElseThrow(() -> new RuntimeException("Requestor not Found"));
    if (requestorerFound != null) {
      requestorerFound.setId(requestor.getId());
      requestorerFound.setNames(requestor.getNames());
      requestorerFound.setId_user(requestor.getId_user());

      return this.requestorRepository.save(requestorerFound);
    }
    return null;
  }
}
