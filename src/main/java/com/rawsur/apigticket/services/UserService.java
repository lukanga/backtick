package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.dto.AuthDto;
import com.rawsur.apigticket.dto.CreateUserDto;
import com.rawsur.apigticket.model.Direction;
import com.rawsur.apigticket.model.User;
import com.rawsur.apigticket.repository.DirectionRepository;
import com.rawsur.apigticket.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
  @Autowired
  private UserRepository userRepository;

  @Autowired
  private DirectionRepository directionRepository;

  public List<User> getAllUser() {
    return userRepository.findAllByOrderByIdDesc();
  }

  public Optional<User> getOneUser(final Integer id) {
    return userRepository.findById(id);
  }

  public User createUser(CreateUserDto userDto) {
    User user = new User();
    user.setEmail(userDto.getEmail());
    user.setNames(userDto.getNames());
    user.setPassword(userDto.getPassword());
    user.setProfil(userDto.getProfil());
    Direction direction = this.directionRepository.findById(userDto.getId_direction())
        .orElseThrow(() -> new RuntimeException("Direction not Found"));
    user.setDirection(direction);

    return userRepository.save(user);
  }

  public User updateUser(User user) {

    User usr = this.userRepository.findById(user.getId()).orElseThrow(() -> new RuntimeException("User not Found"));
    System.out.println(usr);
    if (usr != null) {
      usr.setId(user.getId());
      usr.setEmail(user.getEmail());
      usr.setNames(user.getNames());
      usr.setPassword(user.getPassword());
      return this.userRepository.save(usr);
    }
    return null;
  }

  public User getOneUserByEmailAndPassword(AuthDto authDto) {
    return this.userRepository.findByEmailAndPassword(authDto.getEmail(), authDto.getPassword());
  }
}
