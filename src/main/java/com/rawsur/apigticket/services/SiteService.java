package com.rawsur.apigticket.services;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.dto.CreateSiteDto;
import com.rawsur.apigticket.model.Direction;
import com.rawsur.apigticket.model.Site;
import com.rawsur.apigticket.repository.DirectionRepository;
import com.rawsur.apigticket.repository.SiteRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SiteService {
  @Autowired
  private SiteRepository siteRepository;

  @Autowired
  private DirectionRepository directionRepository;

  public List<Site> getAllSites() {
    return this.siteRepository.findAllByOrderByIdDesc();
  }

  public Optional<Site> getOneSiteById(int id) {
    return this.siteRepository.findById(id);
  }

  public Site creatSite(CreateSiteDto siteDto) {
    Site site = new Site();
    site.setName(siteDto.getName());
    site.setAdress(siteDto.getAdress());
    return this.siteRepository.save(site);
  }

  public Site updateSite(Site site) {
    return this.siteRepository.save(site);
  }

  // public void addOneDirectionOnSite(int id_direction, int id_site) {
  // Direction directionFoud = this.directionRepository.findById(id_direction)
  // .orElseThrow(() -> new RuntimeException("Direction not Found"));
  // Site site = this.siteRepository.findById(id_direction)
  // .orElseThrow(() -> new RuntimeException("Site not Found"));
  // site.getDirections().add(directionFoud);
  // }
}
