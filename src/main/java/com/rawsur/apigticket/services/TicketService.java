package com.rawsur.apigticket.services;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.dto.CreateTicketDto;
import com.rawsur.apigticket.dto.UpdateTicketDto;
import com.rawsur.apigticket.enumaration.StatusTicket;
import com.rawsur.apigticket.model.HistoriqueTicket;
import com.rawsur.apigticket.model.Priority;
import com.rawsur.apigticket.model.Requestor;
import com.rawsur.apigticket.model.Ticket;
import com.rawsur.apigticket.model.TypeIncident;
import com.rawsur.apigticket.model.User;
import com.rawsur.apigticket.repository.HistoriqueTicketRepository;
import com.rawsur.apigticket.repository.PriorityRepository;
import com.rawsur.apigticket.repository.RequestorRepository;
import com.rawsur.apigticket.repository.TicketRepository;
import com.rawsur.apigticket.repository.TypeIncidentRepository;
import com.rawsur.apigticket.repository.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TicketService {
  @Autowired
  private TicketRepository ticketRepository;

  @Autowired
  private HistoriqueTicketRepository historiqueTicketRepository;

  @Autowired
  private TypeIncidentRepository typeIncidentRepository;

  @Autowired
  private PriorityRepository priorityRepository;

  @Autowired
  private RequestorRepository requestorRepository;

  @Autowired
  private UserRepository userRepository;

  public List<Ticket> getAllTicket() {
    return this.ticketRepository.findAllByOrderByIdDesc();
  }

  public List<HistoriqueTicket> getHistoriqueAllTicket() {
    return this.historiqueTicketRepository.getAll();
  }

  public List<Ticket> getAllByStatus(String status) {
    return this.ticketRepository.findAllByStatus(status);
  }

  public List<Ticket> findByintervalOfCreationDate(String startDateIn, String endDateIn) {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    LocalDate startDate = LocalDate.parse(startDateIn, format);
    LocalDate endDate = LocalDate.parse(endDateIn, format);
    return this.ticketRepository.findByintervalOfCreationDate(startDate, endDate);
  }

  public List<Ticket> findByintervalOfProcessDate(String startDateIn, String endDateIn) {
    DateTimeFormatter format = DateTimeFormatter.ofPattern("dd-MM-yyyy");
    LocalDate startDate = LocalDate.parse(startDateIn, format);
    LocalDate endDate = LocalDate.parse(endDateIn, format);
    return this.ticketRepository.findByintervalOfProcessDate(startDate, endDate);
  }

  public Optional<Ticket> getOneTicket(Integer id) {
    return this.ticketRepository.findById(id);
  }

  public Ticket createTicket(CreateTicketDto ticketDto) {

    Ticket ticket = new Ticket();
    ticket.setCriticality_requestor(ticketDto.getCriticality_requestor());
    ticket.setDescription_requestor(ticketDto.getDescription_requestor());
    ticket.setObjet(ticketDto.getObjet());
    ticket.setTitle_requestor(ticketDto.getTitle_requestor());
    ticket.setDelai_resolution_requestor(ticketDto.getDelai_resolution_requestor());
    ticket.setStatus(ticketDto.getStatus().getValue());

    ticket.setCriticality_it(ticketDto.getCriticality_requestor());
    ticket.setTitle_it(ticketDto.getTitle_requestor());
    ticket.setDelai_resolution_it(ticketDto.getDelai_resolution_requestor());
    ticket.setCapture_incident(ticketDto.getCapture_incident());

    // Set Type incident requestor value
    TypeIncident incident_requestor = this.typeIncidentRepository.findById(ticketDto.getId_type_incident_requestor())
        .orElseThrow(() -> new RuntimeException("Ticket requestor not Found"));
    ticket.setType_incident_requestor(incident_requestor);

    // Set priority requestor value
    Priority priority_requestor = this.priorityRepository.findById(ticketDto.getId_priority_requestor())
        .orElseThrow(() -> new RuntimeException("Priority requestor not Found"));
    ticket.setPriority_requestor(priority_requestor);
    ticket.setPriority_it(priority_requestor);

    // Set requestor value
    Requestor requestor = this.requestorRepository.findById(ticketDto.getId_requestor())
        .orElseThrow(() -> new RuntimeException("Requestor not Found"));
    ticket.setRequestor(requestor);

    // Set User create Value
    User user = this.userRepository.findById(ticketDto.getId_user_create())
        .orElseThrow(() -> new RuntimeException("User not Found"));
    ticket.setUser_create(user);

    return this.ticketRepository.save(ticket);
  }

  public Ticket updatePriority(UpdateTicketDto ticket) {

    Ticket ticketFound = this.ticketRepository.findById(ticket.getId())
        .orElseThrow(() -> new RuntimeException("Ticket not Found"));

    if (ticketFound != null) {
      ticketFound.setId(ticket.getId());
      ticketFound.setTitle_it(ticket.getTitle_it());
      ticketFound.setDescription_it(ticket.getDescription_it());
      ticketFound.setCriticality_it(ticket.getCriticality_it());
      ticketFound.setDate_resolve(ticket.getDate_resolve());
      ticketFound.setPriority_it(new Priority(ticket.getId_priority_it()));
      ticketFound.setType_incident_it(new TypeIncident(ticket.getId_type_incident_it()));
      ticketFound.setObservation(ticket.getObservation());
      ticketFound.setStatus(ticket.getStatus().getValue());
      ticketFound.setDelai_resolution_it(ticket.getDelai_resolution_it());
      ticketFound.setUser_update(new User(ticket.getId_user_update()));
      ticketFound.setDate_resolve(LocalDateTime.now());
      ticketFound.setCapture_incident(ticket.getCapture_incident());

      return this.ticketRepository.save(ticketFound);
    }
    return null;
  }
}
