package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.Priority;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PriorityRepository extends JpaRepository<Priority, Integer> {
  public List<Priority> findAllByOrderByIdDesc();
}
