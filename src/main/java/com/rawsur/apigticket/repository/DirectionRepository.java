package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.Direction;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface DirectionRepository extends JpaRepository<Direction, Integer> {

  static final String SQL_ALL = "SELECT * FROM direction WHERE id_site = :id_site";

  @Query(value = SQL_ALL, nativeQuery = true)
  List<Direction> findByIdSite(int id_site);

  public List<Direction> findAllByOrderByIdDesc();
}
