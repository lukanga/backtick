package com.rawsur.apigticket.repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import com.rawsur.apigticket.model.Ticket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

public interface TicketRepository extends JpaRepository<Ticket, Integer> {

  static final String SQL = "SELECT t.id, t.objet, t.title_it, t.title_requestor, t.description_requestor, t.description_it,"
      +
      "t.criticality_requestor, t.criticality_it, t.date_create, t.date_resolve, t.status, t.observation, t.delai, r.names as name_requestor,"
      +
      " ti_rq.name as type_incident_requestor, p_rq.name as priority_requestor FROM ticket t INNER JOIN requestor r ON r.id = t.id_requestor "
      +
      "INNER JOIN type_incident ti_rq ON ti_rq.id = t.id_type_incident_requestor INNER  JOIN priority  p_rq ON p_rq.id=t.id_priority_requestor";

  static final String SQL_FIND_BY_DATE_CREATE = "SELECT * FROM ticket WHERE date_create BETWEEN :startDate AND :endDate";
  static final String SQL_FIND_BY_DATE_PROCESS = "SELECT * FROM ticket WHERE date_resolve BETWEEN :startDate AND :endDate";

  @Query(value = SQL, nativeQuery = true)
  public List<Object> getAllTicket();

  public List<Ticket> findAllByOrderByIdDesc();

  public List<Ticket> findAllByStatus(String status);

  @Query(value = SQL_FIND_BY_DATE_CREATE, nativeQuery = true)
  public List<Ticket> findByintervalOfCreationDate(LocalDate startDate, LocalDate endDate);

  @Query(value = SQL_FIND_BY_DATE_PROCESS, nativeQuery = true)
  public List<Ticket> findByintervalOfProcessDate(LocalDate startDate, LocalDate endDate);
}
