package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.Site;

import org.springframework.data.jpa.repository.JpaRepository;

public interface SiteRepository extends JpaRepository<Site, Integer> {
  public List<Site> findAllByOrderByIdDesc();
}
