package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.HistoriqueTicket;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface HistoriqueTicketRepository extends JpaRepository<HistoriqueTicket, Long> {
  static final String SQL_ALL = "SELECT * FROM v_historique_ticket";

  @Query(value = "SELECT * FROM v_historique_ticket ", nativeQuery = true)
  List<HistoriqueTicket> getAll();

}
