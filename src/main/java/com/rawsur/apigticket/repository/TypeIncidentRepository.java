package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.TypeIncident;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TypeIncidentRepository extends JpaRepository<TypeIncident, Integer> {
  public List<TypeIncident> findAllByOrderByIdDesc();
}
