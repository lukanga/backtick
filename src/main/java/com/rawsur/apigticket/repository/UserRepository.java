package com.rawsur.apigticket.repository;

import java.util.List;

import com.rawsur.apigticket.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Integer> {
  public User findByEmailAndPassword(String email, String password);

  public List<User> findAllByOrderByIdDesc();
}
