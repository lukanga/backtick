package com.rawsur.apigticket.repository;

import com.rawsur.apigticket.model.Requestor;

import org.springframework.data.jpa.repository.JpaRepository;

public interface RequestorRepository extends JpaRepository<Requestor, Integer> {

}
