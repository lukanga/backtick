package com.rawsur.apigticket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApiGticketApplication {

	public static void main(String[] args) {
		SpringApplication.run(ApiGticketApplication.class, args);
	}
}
