package com.rawsur.apigticket.error;

import com.rawsur.apigticket.model.ErrorObjet;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class Errors {
  public static ResponseEntity<?> notFound(String entityName) {
    return new ResponseEntity<>(
        new ErrorObjet(HttpStatus.NOT_FOUND.toString(), entityName + " not found"),
        HttpStatus.NOT_FOUND);
  }
}
