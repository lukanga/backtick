package com.rawsur.apigticket.controller;

import java.util.List;

import com.rawsur.apigticket.dto.CreateDirection;
import com.rawsur.apigticket.model.Direction;
import com.rawsur.apigticket.services.DirectionService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/gest-ticket/direction")
public class DirectionController {
  @Autowired
  private DirectionService directionService;

  @GetMapping("v1/list")
  public List<Direction> getAllDirections() {
    return this.directionService.getAllDirections();
  }

  @GetMapping("v1/list/{id_site}")
  public List<Direction> getDirectionsByIdSite(@PathVariable("id_site") int id_site) {
    return this.directionService.getDirectionsByIdSite(id_site);
  }

  @PostMapping("v1/create")
  public Direction createSite(@RequestBody CreateDirection directionDto) {
    return this.directionService.creatDirection(directionDto);
  }
}
