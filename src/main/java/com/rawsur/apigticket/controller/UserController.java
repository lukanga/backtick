package com.rawsur.apigticket.controller;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import com.rawsur.apigticket.dto.AuthDto;
import com.rawsur.apigticket.dto.CreateUserDto;
import com.rawsur.apigticket.model.User;
import com.rawsur.apigticket.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/gest-ticket/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("v1/list")
    public List<User> getAllUsers() {
        return userService.getAllUser();
    }

    @GetMapping("v1/{id}")
    public Optional<User> getOneUser(@PathVariable("id") Integer id) {
        return userService.getOneUser(id);
    }

    @PostMapping("v1/authentication")
    public User getOneUserByEmailAndPassword(@Valid @RequestBody AuthDto authDto) {
        return userService.getOneUserByEmailAndPassword(authDto);
    }

    @PostMapping("v1/create")
    public User createUser(@RequestBody CreateUserDto userDto) {
        return userService.createUser(userDto);
    }

    @PutMapping("v1/update")
    public User UpdateUser(@RequestBody User user) {
        return userService.updateUser(user);
    }
}
