package com.rawsur.apigticket.controller;

import java.io.File;
import java.io.IOException;
import java.util.Base64;
import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.rawsur.apigticket.routes.Routes;

import org.apache.commons.io.FileUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import lombok.Data;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("test")
public class testController {
  @PostMapping("")
  public ResponseEntity<TestDto> createRequestor(@Valid @RequestBody TestDto dto) {
    return new ResponseEntity<TestDto>(dto, HttpStatus.BAD_REQUEST);
  }

  @PostMapping("img")
  public void saveImg(String filePath, String outputFileName) throws IOException {
    byte[] fileContent = FileUtils.readFileToByteArray(new File(filePath));
    String encodedString = Base64.getEncoder().encodeToString(fileContent);

    byte[] decodedBytes = Base64.getDecoder().decode(encodedString);
    FileUtils.writeByteArrayToFile(new File(outputFileName), decodedBytes);
  }

  @PostMapping("img-test")
  public void testImg(@RequestParam("file") MultipartFile file) throws IOException, IllegalStateException {
    System.out.println(
        "======================================================================================file : " + file);
    file.transferTo(new File(
        "fileUpload\\" + file.getOriginalFilename()));
  }

  @GetMapping("")
  public String index() {
    return "Hello word";
  }
}

@Data
class UserTest {
  private String file;
}

@Data
class UserTest2 {
  private String name;
  private byte[] file;
}

@Data
class TestEntity {
  private UUID id;
  private String name;
  private String email;
  private String password;
  private int age;
}

@Data
class TestDto {
  @NotNull(message = "l'id ne doit pas être vide")
  private UUID id;

  @NotEmpty
  @Size(min = 2, message = "user name should have at least 2 characters")
  private String name;

  @NotEmpty
  @Email
  private String email;

  @NotEmpty
  @Size(min = 8, message = "password should have at least 8 characters")
  private String password;

  @Min(value = 20, message = "Pas de valeur inferieur à 20")
  private int age;
}
