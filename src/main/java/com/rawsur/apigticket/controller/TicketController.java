package com.rawsur.apigticket.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.dto.CreateTicketDto;
import com.rawsur.apigticket.dto.UpdateTicketDto;
import com.rawsur.apigticket.model.HistoriqueTicket;
import com.rawsur.apigticket.model.Ticket;
import com.rawsur.apigticket.services.TicketService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/gest-ticket/ticket")
public class TicketController {
  @Autowired
  private TicketService ticketService;

  @GetMapping("v1/list")
  public List<Ticket> getAllTicket() {
    return ticketService.getAllTicket();
  }

  @GetMapping("v1/all-historique")
  public List<HistoriqueTicket> getAllHistorique() {
    return ticketService.getHistoriqueAllTicket();
  }

  @GetMapping("v1/list-by-status/{status}")
  public List<Ticket> getByStatus(@PathVariable("status") String status) {
    return this.ticketService.getAllByStatus(status);
  }

  @GetMapping("v1/list-by-date-create/{startDate}/{endDate}")
  public List<Ticket> fingByDateCreate(@PathVariable("startDate") String startDateIn,
      @PathVariable("endDate") String endDateIn) {
    return this.ticketService.findByintervalOfCreationDate(startDateIn, endDateIn);
  }

  @GetMapping("v1/list-by-date-process/{startDate}/{endDate}")
  public List<Ticket> fingByDateProcess(@PathVariable("startDate") String startDateIn,
      @PathVariable("endDate") String endDateIn) {
    return this.ticketService.findByintervalOfProcessDate(startDateIn, endDateIn);
  }

  @GetMapping("v1/{id}")
  public Optional<Ticket> getOneTicket(@PathVariable("id") Integer id) {
    return ticketService.getOneTicket(id);
  }

  @PostMapping("v1/create")
  public Ticket createPriority(@RequestBody CreateTicketDto ticket) {
    return ticketService.createTicket(ticket);
  }

  @PutMapping("v1/update")
  public Ticket UpdatePriority(@RequestBody UpdateTicketDto ticket) {
    return ticketService.updatePriority(ticket);
  }
}
