package com.rawsur.apigticket.controller;

import java.util.List;

import com.rawsur.apigticket.dto.CreateSiteDto;
import com.rawsur.apigticket.model.Site;
import com.rawsur.apigticket.services.SiteService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/gest-ticket/site")
public class SiteController {
  @Autowired
  private SiteService siteService;

  @GetMapping("v1/list")
  public List<Site> getAllSite() {
    return this.siteService.getAllSites();
  }

  @PostMapping("v1/create")
  public Site createSite(@RequestBody CreateSiteDto siteDto) {
    return this.siteService.creatSite(siteDto);
  }
}
