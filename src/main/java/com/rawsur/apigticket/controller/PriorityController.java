package com.rawsur.apigticket.controller;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.Priority;
import com.rawsur.apigticket.services.PriorityService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/gest-ticket/priority")
public class PriorityController {
  @Autowired
  private PriorityService priorityService;

  @GetMapping("v1/list")
  public List<Priority> getAllPriority() {
    return priorityService.getAllPriority();
  }

  @GetMapping("v1/{id}")
  public Optional<Priority> getOnePriority(@PathVariable("id") Integer id) {
    return priorityService.getOnePriority(id);
  }

  @PostMapping("v1/create")
  public ResponseEntity<Priority> createPriority(@RequestBody Priority priority) {
    return priorityService.createPriority(priority);
  }

  @PutMapping("v1/update")
  public Priority updatePriority(@RequestBody Priority priority) {
    return priorityService.updatePriority(priority);
  }
}
