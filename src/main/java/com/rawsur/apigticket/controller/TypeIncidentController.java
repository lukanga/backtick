package com.rawsur.apigticket.controller;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.TypeIncident;
import com.rawsur.apigticket.services.TypeIncidentService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/gest-ticket/type-incident")
public class TypeIncidentController {
  @Autowired
  private TypeIncidentService typeIncidentService;

  @GetMapping("v1/list")
  public List<TypeIncident> getAllTypeIncident() {
    return typeIncidentService.getAllTypeIncident();
  }

  @GetMapping("v1/{id}")
  public Optional<TypeIncident> getOneTypeIncident(@PathVariable("id") Integer id) {
    return typeIncidentService.getOneTypeIncident(id);
  }

  @PostMapping("v1/create")
  public TypeIncident createTypeIncident(@RequestBody TypeIncident incident) {
    return typeIncidentService.createTypeIncident(incident);
  }

  @PutMapping("v1/update")
  public TypeIncident UpdateTypeIncident(@RequestBody TypeIncident incident) {
    return typeIncidentService.updateTypeIncident(incident);
  }
}
