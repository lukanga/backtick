package com.rawsur.apigticket.controller;

import java.util.List;
import java.util.Optional;

import com.rawsur.apigticket.model.Requestor;
import com.rawsur.apigticket.services.RequestorService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("api/gest-ticket/requestor")
public class RequestorController {
  @Autowired
  private RequestorService requestorService;

  @GetMapping("v1/list")
  public List<Requestor> getAllRequestor() {
    return requestorService.getAllRequestor();
  }

  @GetMapping("v1/{id}")
  public Optional<Requestor> getOneRequestor(@PathVariable("id") Integer id) {
    return requestorService.getOneRequestor(id);
  }

  @PostMapping("v1/create")
  public Requestor createRequestor(@RequestBody Requestor requestor) {
    return requestorService.createRequestor(requestor);
  }

  @PutMapping("v1/update")
  public Requestor UpdateRequestor(@RequestBody Requestor requestor) {
    return requestorService.updateRequestor(requestor);
  }
}
