package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class AuthDto {
  private String email;
  private String password;
}
