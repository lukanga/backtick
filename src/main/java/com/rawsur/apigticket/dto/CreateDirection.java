package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class CreateDirection {
  private String name;
  private int id_site;
}
