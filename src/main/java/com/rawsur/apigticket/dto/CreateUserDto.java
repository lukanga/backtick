package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class CreateUserDto {
  private String email;
  private String password;
  private String names;
  private int profil;
  private int id_direction;
}
