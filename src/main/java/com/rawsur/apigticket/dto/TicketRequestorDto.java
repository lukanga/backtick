package com.rawsur.apigticket.dto;

import java.time.LocalDateTime;

import lombok.Data;

@Data
public class TicketRequestorDto {
  private int id;
  private String objet;
  private String name_requestor;
  private String type_incident_requestor;
  private String title_requestor;
  private String description_requestor;
  private String priority_requestor;
  private double delai;
  private int criticality_requestor;
  private LocalDateTime date_create;
  private String status;
  private int id_user_create;
  private Integer id_type_incident_it = null;
  private Integer id_priority_it = null;

  private String title_it;
  private String description_it;
  private int criticality_it;
  private LocalDateTime date_resolve;
  private String observation;
}
