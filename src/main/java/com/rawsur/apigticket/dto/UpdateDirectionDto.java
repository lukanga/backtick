package com.rawsur.apigticket.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = false)
public class UpdateDirectionDto extends CreateDirection {
  private int id;
}
