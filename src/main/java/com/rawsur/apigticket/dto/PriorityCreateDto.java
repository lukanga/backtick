package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class PriorityCreateDto {
  private String name;
}
