package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class UserDto {

  @Data
  public class UpdataUserDto {

  }

  @Data
  public class UserCreateDto {
    private String email;
    private String password;
    private String names;
    private int profil;
  }

}
