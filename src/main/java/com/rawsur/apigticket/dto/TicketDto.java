package com.rawsur.apigticket.dto;

import java.time.LocalDateTime;

import com.rawsur.apigticket.enumaration.StatusTicket;

import lombok.Data;
import lombok.EqualsAndHashCode;

class TicketDto {

  @Data
  class CreateTicketDto {
    private String objet;
    private String title_requestor;
    private String description_requestor;
    private int criticality_requestor;
    private LocalDateTime date_create;

    private int id_requestor;
    private int id_type_incident_requestor;
    private int id_priority_requestor;
    private double delai;

    private StatusTicket status;
    private int id_user_create;
  }

  @Data
  @EqualsAndHashCode(callSuper = false)
  class UpdateTicketDto extends CreateTicketDto {
    private String title_it;
    private String description_it;
    private int criticality_it;
    private LocalDateTime date_resolve;
    private String observation;
    private int id_type_incident_it;
    private int id_priority_it;
  }
}
