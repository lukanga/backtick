package com.rawsur.apigticket.dto;

import com.rawsur.apigticket.enumaration.StatusTicket;

import lombok.Data;

@Data
public class CreateTicketDto {
  private String objet;
  private int id_requestor;
  private int id_type_incident_requestor;
  private String title_requestor;
  private String description_requestor;
  private int id_priority_requestor;
  private double delai_resolution_requestor;
  private int criticality_requestor;
  private StatusTicket status;
  private int id_user_create;
  private String capture_incident;
}
