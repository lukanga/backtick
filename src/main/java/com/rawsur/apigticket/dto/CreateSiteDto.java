package com.rawsur.apigticket.dto;

import lombok.Data;

@Data
public class CreateSiteDto {
  private String name;
  private String adress;
}
