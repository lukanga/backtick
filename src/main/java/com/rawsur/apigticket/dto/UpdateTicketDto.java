package com.rawsur.apigticket.dto;

import java.time.LocalDateTime;

import com.rawsur.apigticket.enumaration.StatusTicket;

import lombok.Data;

@Data
public class UpdateTicketDto {
  private int id;
  private String title_it;
  private String description_it;
  private int criticality_it;
  private LocalDateTime date_resolve;
  private String observation;
  private int id_type_incident_it;
  private int id_priority_it;
  private double delai_resolution_it = -1;
  private StatusTicket status;
  private int id_user_update;
  private String capture_incident;
}
