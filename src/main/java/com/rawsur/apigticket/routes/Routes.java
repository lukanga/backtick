package com.rawsur.apigticket.routes;

public final class Routes {
  public static String baseUri = "api/gest-ticket";
  public static String userUri = "/user";
  public static String ticketUri = "/ticket";
  public static String priorityUri = "/priority";
  public static String requestorUri = "/requestor";
  public static String typeIncidentUri = "/type-incident";
  public static String testUri = "/test";
}
